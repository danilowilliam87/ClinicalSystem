package io.github.danilowilliam.ClinicalSystem.model;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
public class Medico {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nome;
    private String cpf;
    private String crm;
    private String senha;
    @OneToOne(cascade = CascadeType.ALL)
    private Especialidade especialidade;

    @OneToMany(mappedBy = "medico",orphanRemoval = false)
    @ToString.Exclude
    private List<Consulta>consultas;

    public Medico(List<Consulta>consultas){
        this.consultas = consultas;
    }

    public Medico(String nome,String cpf,String crm,String senha,Especialidade especialidade){
        this.nome = nome;
        this.senha = senha;
        this.cpf = cpf;
        this.crm = crm;
        this.especialidade = especialidade;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Medico medico = (Medico) o;
        return id != null && Objects.equals(id, medico.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}

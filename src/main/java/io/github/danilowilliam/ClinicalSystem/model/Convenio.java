package io.github.danilowilliam.ClinicalSystem.model;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
public class Convenio {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    @NotBlank
    private String nome;
    @NotBlank
    @Column(nullable = false)
    private String tipo;

    public Convenio(String nome, String tipo){
        this.nome = nome;
        this.tipo = tipo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Convenio convenio = (Convenio) o;
        return id != null && Objects.equals(id, convenio.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}

package io.github.danilowilliam.ClinicalSystem.exceptions;

import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class ControllerAdvice {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiErrors handleValidationErrors(MethodArgumentNotValidException ex){
        BindingResult bindingResult = ex.getBindingResult();
      List<String> messages =   bindingResult.getAllErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toList());
          return new ApiErrors(messages);
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ResponseError> resourceNotFind(ResourceNotFoundException exception,
                                                         HttpServletRequest request){
        ResponseError responseError = new ResponseError()
                .setTime(Instant.now().toString())
                .setErrorMessage(exception.getMessage())
                .setPath(request.getRequestURI())
                .setStatusCode(String.valueOf(HttpStatus.NOT_FOUND.value()));

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(responseError);
    }


    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<ResponseError> requestBodyMissingexception(HttpMessageNotReadableException exception,
                                                                     HttpServletRequest request){
        ResponseError responseError = new ResponseError()
                .setTime(Instant.now().toString())
                .setErrorMessage(exception.getLocalizedMessage().substring(0, 32))
                .setPath(request.getRequestURI())
                .setStatusCode(String.valueOf(HttpStatus.BAD_REQUEST.value()));

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseError);
    }


    //MethodArgumentTypeMismatchException
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<ResponseError> urlNotFound(HttpServletRequest request){
        ResponseError responseError = new ResponseError()
                .setTime(Instant.now().toString())
                .setErrorMessage("URL NOT FOUND")
                .setPath(request.getRequestURI())
                .setStatusCode(String.valueOf(HttpStatus.BAD_REQUEST.value()));

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseError);
    }


    @ExceptionHandler(DataIntegrityException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<ResponseError> dataIntegrityException(HttpServletRequest request,DataIntegrityException exception){
        ResponseError responseError = new ResponseError()
                .setTime(Instant.now().toString())
                .setErrorMessage(exception.getMessage())
                .setPath(request.getRequestURI())
                .setStatusCode(String.valueOf(HttpStatus.BAD_REQUEST.value()));

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseError);
    }


    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    public ResponseEntity<ResponseError> methodNotAlowedException(HttpServletRequest request, HttpRequestMethodNotSupportedException exception){
        ResponseError responseError = new ResponseError()
                .setTime(Instant.now().toString())
                .setErrorMessage(exception.getMessage())
                .setPath(request.getRequestURI())
                .setStatusCode(String.valueOf(HttpStatus.METHOD_NOT_ALLOWED.value()));

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseError);
    }


    //DataIntegrityException
}

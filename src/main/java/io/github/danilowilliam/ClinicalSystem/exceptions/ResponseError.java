package io.github.danilowilliam.ClinicalSystem.exceptions;


public class ResponseError {
    private String time;
    private String statusCode;
    private String errorMessage;
    private String path;

    public ResponseError() {
    }

    public ResponseError(String time, String statusCode,  String errorMessage, String path) {
        this.time = time;
        this.statusCode = statusCode;

        this.errorMessage = errorMessage;
        this.path = path;
    }

    public String getTime() {
        return time;
    }

    public ResponseError setTime(String time) {
        this.time = time;
        return this;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public ResponseError setStatusCode(String statusCode) {
        this.statusCode = statusCode;
        return this;
    }


    public String getErrorMessage() {
        return errorMessage;
    }

    public ResponseError setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
        return this;
    }

    public String getPath() {
        return path;
    }

    public ResponseError setPath(String path) {
        this.path = path;
        return this;
    }
}

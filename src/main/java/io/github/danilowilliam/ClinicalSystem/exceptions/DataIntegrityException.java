package io.github.danilowilliam.ClinicalSystem.exceptions;

public class DataIntegrityException extends RuntimeException{

    public DataIntegrityException(String message) {
        super(message);
    }
}

package io.github.danilowilliam.ClinicalSystem.config;

import io.github.danilowilliam.ClinicalSystem.model.Convenio;
import io.github.danilowilliam.ClinicalSystem.model.Endereco;
import io.github.danilowilliam.ClinicalSystem.model.Funcionario;
import io.github.danilowilliam.ClinicalSystem.model.Paciente;
import io.github.danilowilliam.ClinicalSystem.services.ConvenioService;
import io.github.danilowilliam.ClinicalSystem.services.EnderecoService;
import io.github.danilowilliam.ClinicalSystem.services.FuncionarioService;
import io.github.danilowilliam.ClinicalSystem.services.PacienteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;

@Configuration
public class LoadData implements ApplicationRunner {

    Logger LOGGER = LoggerFactory.getLogger(LoadData.class);

    private final Convenio convenio = new Convenio("BRADESCO SAUDE", "EMPRESARIAL");
    private final Convenio convenio2 = new Convenio("GOLDENCROSS", "EMPRESARIAL");
    private final Convenio convenio3 = new Convenio("MEDIAL SAUDE","EMPRESARIAL");

    private final Funcionario funcionario = new Funcionario("JOAO FONSECA","123", "joao@email.com","A123","333","ADMIN");

    private final Funcionario funcionario2 = new Funcionario("PEDRO DE JESUS","456", "pedro@email.com","B123","111","ADMIN");

    private final Endereco endereco1 = new Endereco("40230000","RUA SEM NOME",88,
            "MIRACEMA DO NORTE","TOCANTINS");
    private final Endereco endereco2 = new Endereco("89530000","RUA SR DOS ANEIS",199898,
            "JABOATA DOS GRUARARAPES","SAO PAULO");
    private final  Paciente paciente1 = new Paciente("JOAO DE JESUS",
            "joao@email.com","45224201020", "7198989898",LocalDate.of(2000,1,1),
            endereco1,convenio);
    private final Paciente paciente2 = new Paciente("MARIELA DE JESUS",
            "mariela@email.com","65626839046", "7198989855",LocalDate.of(2000,1,1),
            endereco2,convenio2);



    @Autowired
    private ConvenioService convenioService;

    @Autowired
    private FuncionarioService funcionarioService;

    @Autowired
    private PacienteService pacienteService;

    @Autowired
    private EnderecoService enderecoService;

    public void saveConvenio(){
        convenioService.salvar(convenio);
        convenioService.salvar(convenio2);
        convenioService.salvar(convenio3);
        LOGGER.info("objetos convenio salvo com sucesso");
    }

    public void saveFuncionarios(){
        funcionarioService.salvar(funcionario);
        funcionarioService.salvar(funcionario2);
        LOGGER.info("objetos funcionario salvo com sucesso");
    }


    public void salvaPaciente(){
        pacienteService.salvar(paciente1);
        pacienteService.salvar(paciente2);
        LOGGER.info("objetos paciente salvo com sucesso");
    }

    public void salvarEndereco(){
        enderecoService.salvar(endereco1);
        enderecoService.salvar(endereco2);
        LOGGER.info("objetos endereco salvo com sucesso");
    }



    @Override
    public void run(ApplicationArguments args) throws Exception {
        saveConvenio();
        saveFuncionarios();
        salvarEndereco();
        salvaPaciente();
    }
}

package io.github.danilowilliam.ClinicalSystem;

import io.github.danilowilliam.ClinicalSystem.exceptions.ResourceNotFoundException;
import io.github.danilowilliam.ClinicalSystem.model.Convenio;
import io.github.danilowilliam.ClinicalSystem.repository.ConvenioRepository;
import io.github.danilowilliam.ClinicalSystem.services.ConvenioService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class ConvenioTest {

    private Convenio convenio;
    private Convenio convenio2;

    @Autowired
    private ConvenioService convenioService;

    @Autowired
    private ConvenioRepository convenioRepository;

    @BeforeEach
    public void load(){
        convenio = new Convenio();
        convenio2 = new Convenio();
    }

    @Test
    public void consultarConvenioTest(){
        convenio = convenioService.busca(2L);
        Assertions.assertEquals("GOLDENCROSS", convenio.getNome());
    }

    @Test
    public void salvarConvenioTest(){
        convenio2.setNome("HAP VIDA");
        convenio2.setTipo("REDE PRÓPRIA");
        convenioService.salvar(convenio2);
        Assertions.assertEquals("HAP VIDA", convenioService.busca(4L).getNome());
    }

    @Test
    public void buscarTodosConveniosTest(){
        List<Convenio> convenios = new ArrayList<>();
        convenios = convenioRepository.findAll();
        Assertions.assertEquals(3, convenios.size());
    }

    @Test
    public void atualizarConvenioTest(){
        convenio = convenioService.busca(2L);
        convenio.setNome("GOLDEN CROSS COMPAHIA DE SEGUROS");
        convenioService.atualizar(convenio, convenio.getId());
        Convenio busca = convenioService.busca("GOLDEN CROSS COMPAHIA DE SEGUROS");
        Assertions.assertEquals(2L, busca.getId());
    }

    @Test
    public void deletarConvenioTest(){
        convenio2.setNome("HAP VIDA");
        convenio2.setTipo("REDE PRÓPRIA");
        convenioService.salvar(convenio2);
        convenioService.deletar(4L);
        String mensagemDeErro = Assertions.assertThrows(ResourceNotFoundException.class, ()->{
            convenioService.busca(4L);
        }).getMessage();

        Assertions.assertEquals("NOT_FOUND", mensagemDeErro);
    }
}
